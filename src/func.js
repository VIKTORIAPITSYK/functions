function getSum(arg1, arg2) {
  if (typeof arg1 !== 'string'
  || typeof arg2 !== 'string'
  ||isNaN(arg1)
  || isNaN(arg2)) {
    return false
  }
  let arr = [...arguments];
  arr.sort((x, y) => x.length - y.length);
  let shorterStr = arr[0].length;
  let rest = arr[1].slice(shorterStr);
  let result = '';
  for (let i=0; i<shorterStr; i++) {
    result += (Number(arg1[i]) + Number(arg2[i]));
  }
  return result + rest
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let countOfPosts = 0;
  let countOfComents = 0;
  listOfPosts.forEach(x => {
    if (x.author === authorName) countOfPosts++;
    if (x.comments) {
      x.comments.forEach(com => {
        if (com.author === authorName) countOfComents++
      })
    }
  })
  return `Post:${countOfPosts},comments:${countOfComents}`
};

const tickets=(people)=> {
  var a25 = 0,a50 = 0;
  for(var i = 0;i<people.length;i++){
    if(people[i] == 25){
      a25 += 1;
    }
    if(people[i] == 50){
      a25 -= 1; a50 += 1;
    }
    if(people[i] == 100){
      if(a50 == 0 && a25 >= 3){
        a25 -= 3;
      }else{
        a25 -= 1; a50 -= 1;
      }
    }
    if(a25 < 0 || a50 < 0){
      return 'NO';
    }
  }
  return 'YES';
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
